// db.users.insertOne({
        
//         "username": "dahyunKim",
//         "password": "once1234"
    
// })

// db.users.insertOne({
    
//         "username": "teejaecalinao",
//         "password": "tjc1234"
    
// })

// db.users.insertMany([
    
//     {
//         "username":"pablo123",
//         "password":"123paul"
//     },
//     {
//         "username":"pedro99",
//         "password":"iampeter99"
//     }

// ])

//Commands for Creating Documents:
/*
        db.collection.insertOne({
                
                "field1":"value",
                "field2": "value"

        })

        db.collection.insertMany([

                {
                   "field1": "valueA",
                   "field2": "valueA"
                },
                {
                   "field1": "valueB",
                   "field2": "valueB"
                }


        ])

*/
//================= READ ==============

//find() - gets all documents that matches the criteria
//findOne() - gets the first document that matches that criteria

//db.collection.find() - return/find all documents in the collection.
//db.users.find()
//db.products.find()


//db.collection.find({"criteria":"value"}) - returns/find all documents that match the criteria
//db.users.find({"username":"pedro99"})

//Mini-Activity:
//1. Retrieve/find all car documents.
//2. Retrieve/find all car documents which type is "sedan".
//Screenshot your output and send it in the hangouts

// db.cars.find()
// db.cars.find({type:"sedan"})

//db.collection.findOne({}) - find/return the first item/document in the collection.
//db.cars.findOne({})

//db.collection.findOne({"criteria":"value"}) - find/return the first item/document that matches the criteria
//db.cars.findOne({type: "sedan"})

//=================== UPDATE ==============

//updateOne() - allows us update the first item that matches our criteria
//db.collection.updateOne({criteria:"value"},{$set:{fieldToBeUpdated:"Updated Value"}})
//db.users.updateOne({username:"pedro99"},{$set:{username:"peter1999"}})

//db.collection.updateOne({},{$set:{"fieldToBeUpdated":"Updated Value"})
//Allows us to update the first item in the collection
//db.users.updateOne({},{$set:{username:"dahyunieTwice"}})

//If the field being updated does not yet exist, mongodb will instead 
//add that field into the document.
//db.users.updateOne({username:"pablo123"},{$set:{isAdmin:true}})

//updateMany() - allows us to update all documents that matches that criteria
//db.collection.updateMany({},{$set:{"fieldToUpdated":"Update Value"}})
//Allows us to update all items in the collection.

//db.users.updateMany({},{$set:{isAdmin:true}})

//db.collection.updateMany(({"criteria":"value"},{$set:{"fieldToBeUpdated":"Updated Value"}})
//Allows us to update all items that matches our criteria
//db.cars.updateMany({type:"sedan"},{$set:{price:1000000}})

//================ DELETE ===============

//deleteOne() - deletes the first document that matches the criteria
//db.collection.deleteOne({}) - deletes first item in collection
//db.users.deleteOne({})

//db.collection.deleteOne({"criteria":"criteria"})
//deletes first item that matches criteria
//db.cars.deleteOne({brand:"Toyota"})

//deleteMany() - deletes all items that matches the criteria

//db.collection.deleteMany({"criteria":"value"})
//deletes all items that matches the criteria
//db.users.deleteMany({isAdmin:true})

//db.collection.deleteMany({})
//delete all documents in a collection
db.cars.deleteMany({})
